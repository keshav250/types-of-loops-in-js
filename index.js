// There are 7 kind of loops you will find in JavaScript.

// while
// do-while
// for
// forEach()
// map()
// for…in
// for…of

// while loop

while (i<10){
 console.log("I is less than 10");
 i++;
}

// Output
// I is less than 10
// I is less than 10
// ... 10 times
// In the above example, the condition is getting checked if the value of i is less than 10 or not. If the condition is true, the block of code gets executed and before iterating next time the value of i is getting increases by 1 as we’ve added a statement i++.

// do-while

while (condition);
Example
var i=7;
do{
    console.log("The value of i is " + i);
    i++;
}
while(i>7 && i<10);

// Output
// The value of i is 7
// The value of i is 8
// The value of i is 9
// As you can see the condition is- *the value of i is greater than 7 but less than 10; but in output the value of i=7 has been printed. Because this looping technique first do execute the code irrespective of the condition and then compares the condition from 2nd round of execution. For all the true condition from the 2nd looping round the code block will be executed.

// for loop

Example
for (var i=0; i<10; i++){
    console.log("The Value of i Is " + i);
}

// Output
// The Value of i Is 0
// The Value of i Is 1
// ...
// The Value of i Is 9
// Take a look at the above example, the initialization, condition & increment has been declared in a single line. It’s easier to understand and better readable. Isn’t it ?



// forEach()

var arr=[10, 20, "hi", ,{}];

arr.forEach(function(item, index){
    console.log(' arr['+index+'] is '+ item);
});

// Output
// arr[0] is 10
// arr[1] is 20
// arr[2] is hi
// arr[4] is [object Object]
// The forEach() method is iterating over the arr array. If you have no use of the index, you can only use arr.forEach(function(item){}).The parameters can be used accordingly, you don’t have to mention all three every time.

// map()

var num = [1, 5, 10, 15];
var doubles = num.map(function(x) {
   return x * 2;
});
// In the above example the new array named doubles will be filled with the outputs doubles=[2, 10, 20, 30] and num array is still [1, 5, 10, 15].

// for…in

Example
var obj = {a: 1, b: 2, c: 3};    
for (var prop in obj) {
    console.log('obj.'+prop+'='+obj[prop]);
};

// Output
// obj.a=1
// obj.b=2
// obj.c=3
// Why array iteration using for...in loop is not preferable?
// for...in should not be used for Array iteration specially where the order of the index is important.
// Actually there is no difference between array indexes and a general object property, array indexes are just enumerable properties.

// The for...in won’t return the indexes in any particular order every time. for...in iterate This loop will return all enumerable properties, including those with non–integer names and those that are inherited.

// So it is recommended to use for or forEach() while iterating over an array. Because the order of iteration is implementation-dependent and iterating over an array may not visit elements in a consistent order in case of for...in.

// for…of

var str= 'paul';
for (var value of str) {
console.log(value);
}

// Output
// "p"
// "a"
// "u"
// "l"